#!/bin/sh
#
# Deploys the application.
#

#fail on error (-e). fail on variable unset (-u).
set -o nounset
set -o errexit

usage()
{
cat << EOF
usage: $0 [-c] [-h] -p profile

This script deploys the application to the given environment.

OPTIONS:
   -p profile     The profile to use: "test" or "prod".
   -c             Checkout latests sources before deploying.
   -h             Perform a Hot Deploy in Tomcat (do not restart Tomcat).
EOF
}

CHECKOUT=false
PROFILE=
HOTDEPLOY=false

while getopts "chp:" OPTION
do
    case $OPTION in
	c)
	    CHECKOUT=true
	    ;;
	h)
	    HOTDEPLOY=true
	    ;;
	p)
	    PROFILE=$OPTARG
	    ;;
	?)
	    usage
	    exit
	    ;;
    esac
done

# Profile is mandatory and can only be "test" or "prod"
if [[ -z $PROFILE || (($PROFILE != "test") && ($PROFILE != "prod")) ]]; then
    usage
    exit 1
fi

if [[ $HOTDEPLOY == false ]]; then
    service tomcat stop
fi

if [[ $CHECKOUT == true ]]; then
    ./checkout.sh
fi

./build.sh -p $PROFILE

echo ========== Depoying to $PROFILE environment ==========

if [[ $PROFILE == "test" ]]; then
    cp -v sources/mucontacts/contactmanager/target/testing.war /usr/local/apache-tomcat/webapps_mucontacts
    if [[ $HOTDEPLOY == false ]]; then
        rm -rf /usr/local/apache-tomcat/webapps_mucontacts/testing
    fi
    ./prepareTestDB.sh
fi


if [[ $PROFILE == "prod" ]]; then
    cp -v sources/mucontacts/contactmanager/target/mucontacts.war /usr/local/apache-tomcat/webapps_mucontacts
    if [[ $HOTDEPLOY == false ]]; then
        rm -rf /usr/local/apache-tomcat/webapps_mucontacts/mucontacts
    fi
fi


if [[ $HOTDEPLOY == false ]]; then
    service tomcat start
fi
