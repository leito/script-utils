#!/bin/sh
#
# Builds the project for the given profile.
#
# Usage: build.sh PROFILE
#    PROFILE: "test" or "prod".
#

#fail on error (-e). fail on variable unset (-u). 
set -o nounset
set -o errexit

usage()
{
cat << EOF
usage: $0 -p profile

This scripts builds the application with the given profile.

OPTIONS:
   -p profile     The profile to use: "test" or "prod".
EOF
}

PROFILE=

while getopts "p:" OPTION
do
    case $OPTION in
	p)
	    PROFILE=$OPTARG
	    ;;
    esac
done

if [[ -z $PROFILE || (($PROFILE != "test") && ($PROFILE != "prod")) ]]; then
    usage
    exit 1
fi

echo ========== Building for $PROFILE environment ==========
cd sources/mucontacts/contactmanager
mvn -Dmaven.test.skip=true -P$PROFILE clean package
