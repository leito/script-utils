#!/bin/sh
#
# Displays tails of log.
# Usage: tailLog.sh PROFILE
#    PROFILE: "test" or "prod".
#

#fail on error (-e). fail on variable unset (-u).
set -o nounset
set -o errexit

usage()
{
cat << EOF
usage: $0 -p profile

This script tails the log of the application in the given environment.

OPTIONS:
   -p profile     The profile to use: "test" or "prod".
EOF
}

PROFILE=

while getopts "chp:" OPTION
do
    case $OPTION in
	p)
	    PROFILE=$OPTARG
	    ;;
	?)
	    usage
	    exit
	    ;;
    esac
done


if [[ $PROFILE == "test" ]]; then
    LOG_FILE=/usr/local/apache-tomcat/logs/mucontacts-test/mucontacts.log
elif [[ $PROFILE == "prod" ]]; then
    LOG_FILE=/usr/local/apache-tomcat/logs/mucontacts/mucontacts.log
else
    usage
    exit 1
fi

tail -500f $LOG_FILE
