#!/bin/sh
#
# Deploys the application.
#

#fail on error (-e). fail on variable unset (-u).
set -o nounset
set -o errexit

usage()
{
cat << EOF
usage: $0 [-c] [-h] -p profile

This script deploys the application to the given environment.

OPTIONS:
   -c             Checkout latests sources before deploying.
   -h             Perform a Hot Deploy in Tomcat (do not restart Tomcat).
EOF
}

CHECKOUT=false
HOTDEPLOY=false

while getopts "chp:" OPTION
do
    case $OPTION in
        c)
            CHECKOUT=true
            ;;
        h)
            HOTDEPLOY=true
            ;;
        ?)
            usage
            exit
            ;;
    esac
done

if [[ $HOTDEPLOY == false ]]; then
    service tomcat stop
fi

if [[ $CHECKOUT == true ]]; then
    ./checkout.sh
fi

./build.sh

echo ========== Deploying ==========
cp -v sources/alto-gamer-web/target/alto-gamer-web-1.0.war /usr/local/apache-tomcat/webapps_altogamer/ROOT.war
if [[ $HOTDEPLOY == false ]]; then
    rm -rf /usr/local/apache-tomcat/webapps_altogamer/ROOT
fi


if [[ $HOTDEPLOY == false ]]; then
    service tomcat start
fi