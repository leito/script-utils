#!/bin/sh
#
# Downloads the latests revision of sources files.
#
# Usage: checkout.sh
#

#fail on error (-e). fail on variable unset (-u).
set -o nounset
set -o errexit

echo ========== Checking out sources =========
rm -rf sources
mkdir sources
cd sources
#git clone https://leito@bitbucket.org/ideasagiles/unoreader.git
git clone git@bitbucket.org:altogamer/alto-gamer-web.git
cd ..
